<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'name',
        'cover',
        'path_to_storage'


    ];

    protected $dates = [
        'deletedAt'
    ];

    public function songs()
    {
        return $this->hasMany(Song::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
