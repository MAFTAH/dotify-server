<?php

namespace App\Http\Controllers;

use App\Album;
use App\Song;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class SongController extends Controller
{
    public function getAllSongs()
    {
        $songs = Song::all();
        return response()->json([
            'songs' => $songs
        ], 200);
    }

    public function getSong($songId)
    {
        $song = Song::find($songId);

        if (!$song) {
            return response()->json([
                'error' => 'Song not found'
            ], 404);
        }
        return response()->json([
            'song' => $song
        ], 200);
    }

    public function postSong(Request $request, $albumId)
    {

        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => 'Album not found'
            ], 404);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'cover_file' => 'required',
            'song_file' => 'required',
            'viewers' => 'required',

        ]);

        // validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }

        if ($request->hasFile('song_file')) {
            $this->songPath = $request->file('song_file')->store('songs');
        } else return response()->json([
            'message' => 'Add a song'
        ], 404);

        if ($request->hasFile('cover_file')) {
            $this->coverPath = $request->file('cover_file')->store('covers');
        } else return response()->json([
            'message' => 'Add a cover'
        ], 404);


        $song = new Song();
        $song->title = $request->input('title');
        $song->song_file = $this->songPath;
        $song->cover_file = $this->coverPath;
        $song->viewers = 89;
        $song->genre_id = 1;

        $album->songs()->save($song);

        return response()->json([
            'song' => $song
        ], 200);
    }

    public function putSong(Request $request, $songId)
    {
        // Find a song
        $song = Song::find($songId);
        if (!$song) {
            return response()->json([
                'error' => 'Song not found'
            ], 404);
        }
        // Validation of request input
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'viewers' => 'required',
            'genre_id' => 'required',
        ]);

        // If validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }

        $song->update([
            'title' => $request->input('title'),
            'viewers' => $request->input('viewers'),
            'genre_id' => $request->input('genre_id'),
        ]);
    }

    public function deleteSong($songId)
    {
        $song = Song::findOrFail($songId);

        if (!$song) {
            return response()->json([
                'error' => 'Song not found'
            ], 404);
        }

        $song->delete();

        return response()->json([
            'message' => 'Song deleted successfully'
        ], 200);
    }

    public function viewSongFile($songId)
    {
        $song = Song::find($songId);
        if (!$song) {
            return response()->json([
                'error' => 'Song not exists'
            ], 404);
        }
        $pathToFile = storage_path('/app/' . $song->song_file);
        return response()->download($pathToFile);
    }

    public function viewCoverFile($songId)
    {
        $song = Song::find($songId);
        if (!$song) {
            return response()->json([
                'error' => 'Song not found'
            ], 404);
        }

        $pathToFile = storage_path('/app/' . $song->cover_file);
        return response()->download($pathToFile);
    }
}
