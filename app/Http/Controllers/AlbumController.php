<?php

namespace App\Http\Controllers;

use App\Album;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class AlbumController extends Controller
{
    // Get all albums
    public function getAllAlbums()
    {
        $albums = Album::all();
        foreach ($albums as $album) {
            $album->songs;
        }
        return response()->json([
            'albums' => $albums
        ], 200);
    }

    // Get a single album
    public function getAlbum($albumId)
    {
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => "Album not found"
            ], 404);
        }
        $album->songs;
        return response()->json([
            'album' => $album
        ], 200);
    }

    // Post an album to database
    public function postAlbum(Request $request, $userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json([
                'error' => "User not found"
            ], 404);
        }

        $this->path = null;

        // Validate if the request sent contains this parameters
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'cover' => 'required',

        ]);


        // If validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }

        $path_to_storage = 'albums/' . $user->name . '_' .$user->id . '_albums/'.$request->input('name');

        if ($request->hasFile('cover')) {
            $this->path = $request->file('cover')->store($path_to_storage.'/cover');
        }


        $album = new Album();
        $album->name = $request->input('name');
        $album->cover = $this->path;
        $album->path_to_storage = $path_to_storage;

        // Save the Album
        $user->albums()->save($album);

        return response()->json([
            'album' => $album
        ], 201);
    }

    // Edit album in database
    public function putAlbum(Request $request, $albumId)
    {
        // Validate if the request sent contains this parameters
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'user_id' => 'required',

        ]);

        // If validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }

        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => "Album not found"
            ], 404);
        }

        $album->update([
            'name' => $request->input('name'),
            // 'artist_id' => $request->input('artist_id'),
        ]);

        $album()->save();

        return response()->json([
            'album' => $album
        ], 206);
    }

    // delete album from database
    public function deleteAlbum($albumId)
    {
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => 'Album not exists'
            ], 204);
        }

        $album->delete();
        return response()->json([
            'album' => 'Album deleted successfully'
        ], 200);
    }

    public function viewFile($albumId)
    {
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => 'Album not exists'
            ], 204);
        }
        $pathToFile = storage_path('/app/' . $album->cover);
        return response()->download($pathToFile);
    }
}
