<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Song extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'viewers',
        'song_file',
        'cover_file',
        'is_top_hit',
        'is_viral',
        'is_chill_acostic',
        'album_id',
        'genre_id',

    ];

    protected $dates = [
        'deletedAt'
    ];

    public function album()
    {
        return  $this->belongsTo(Album::class);
    }

    public function genre()
    {
        return  $this->belongsTo(Genre::class);
    }
}
