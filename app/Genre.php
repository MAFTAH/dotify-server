<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Genre extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'name',
        'icon',


    ];

    protected $dates = [
        'deletedAt'
    ];

    public function songs(){
        return $this->hasMany(Song::class);
    }
}
