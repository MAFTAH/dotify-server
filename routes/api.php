<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Routes for Users
Route::post('login', ['uses' => 'UserController@login']);
Route::post('logout', ['uses' => 'UserController@logout']);
Route::get('users', ['uses' => 'UserController@getAllUsers']);
Route::get('user/{userId}', ['uses' => 'UserController@getUser']);
Route::put('user/{userId}', ['uses' => 'UserController@putUser']);
Route::post('register', ['uses' => 'UserController@registerUser']);
Route::delete('user/{userId}', ['uses' => 'UserController@deleteUser']);
Route::post('user/{userId}', ['uses' => 'UserController@changeUserRole']);


//  Routes for Song
Route::get('songs', ['uses' => 'SongController@getAllSongs']);
Route::post('song/{albumId}', ['uses' => 'SongController@postSong']);
Route::get('song/{songId}', ['uses' => 'SongController@getSong']);
Route::put('song/{songId}', ['uses' => 'SongController@putSong']);
Route::delete('song/{songId}', ['uses' => 'SongController@deleteSong']);

//  Routes for Album
Route::get('albums', ['uses' => 'AlbumController@getAllAlbums']);
Route::post('album/{userId}', ['uses' => 'AlbumController@postAlbum']);
Route::get('album/{albumId}', ['uses' => 'AlbumController@getAlbum']);
Route::put('album/{albumId}', ['uses' => 'AlbumController@putAlbum']);
Route::delete('album/{albumId}', ['uses' => 'AlbumController@deleteAlbum']);


// Routes for Viewing Files
Route::get('song/cover_file/{songId}', ['uses' => 'SongController@viewCoverFile']);
Route::get('album/cover/{albumId}', ['uses' => 'AlbumController@viewFile']);
Route::get('song/song_file/{songId}', ['uses' => 'SongController@viewSongFile']);
