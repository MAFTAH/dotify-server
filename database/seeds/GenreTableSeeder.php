<?php

use App\Genre;
use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Pop Genre
        $genre = new Genre();
        $genre -> name = 'POP';
        $genre -> icon = 'pop_mic_icon@3x.png';
        $genre -> save();

        // Rock Genre
        $genre = new Genre();
        $genre -> name = 'ROCK';
        $genre -> icon = 'rock_fire_icon@3x.png';
        $genre -> save();

         // Rock Genre
         $genre = new Genre();
         $genre -> name = 'SINGELI';
         $genre -> icon = 'radio_icon@3x.png';
         $genre -> save();

          // Rock Genre
        $genre = new Genre();
        $genre -> name = 'REGGER';
        $genre -> icon = 'settings_btn@3x.png';
        $genre -> save();
    }
}
