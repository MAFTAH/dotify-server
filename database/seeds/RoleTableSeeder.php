<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Admin Role
        $role = new Role();
        $role->name = 'Admin';
        $role->description = 'A system Administrator';
        $role->save();


        // Artist Role
        $role = new Role();
        $role->name = 'Artist';
        $role->description = 'Owner of the song,, The one who has the right to upload the song';
        $role->save();


        // Listerner Role
        $role = new Role();
        $role->name = 'Listerner';
        $role->description = 'Huyu ni shabiki (Baharia)';
        $role->save();


    }
}
