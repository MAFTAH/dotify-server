<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('title');
            $table->bigInteger('viewers');
            $table->String('cover_file');
            $table->String('song_file');
            $table->bigInteger('album_id');
            $table->boolean('is_top_hit')->nullable();
            $table->boolean('is_chill_acoustic')->nullable();
            $table->boolean('is_viral')->nullable();
            $table->integer('genre_id');
            $table->softDeletes();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
