<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Album;
// use App\Model;
use Faker\Generator as Faker;

$factory->define(Album::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'cover' => $faker->text(10),
        'artist_id' => $faker->randomNumber(3),
    ];
});
